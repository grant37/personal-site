var csrf = require('csurf');

module.exports = function(req, res, next) {
 
  var whitelist = [
    '/exif',
    '/pgp'
  ];
  
  if (whitelist.indexOf(req.url) !== -1) {
    next();
  }
  else {
    (csrf())(req, res, next);
  }
};