;(function(window) {
  
  var F = kbpgp["const"].openpgp;
  
  var app = angular.module('pgp', ['ngRoute']);
  
  app.config(function($routeProvider, $locationProvider) {
    
    $routeProvider
      .when("/generate", {
        templateUrl: "views/generate.html",
        controller: "generateController"
      })
      .when("/encrypt", {
        templateUrl: "views/encrypt.html",
        controller: "encryptController"
      })
      .when("/decrypt", {
        templateUrl: "views/decrypt.html",
        controller: "decryptController"
      })
      .otherwise({
        redirectTo: "/generate"
    });
    
    $locationProvider.html5Mode(true);
  });
  
  app.controller("navController", ["$scope", "$route", "$location", function($scope, $route, $location) {
    $scope.isPath = function(path) {
      return $location.path() == path;
    };
  }]);
  
  app.controller("generateController", ["$scope", "$route", "$location", function($scope, $route, $location) {
    $scope.keysize = 2048;
    $scope.expires = 0;
    $scope.generating = false;
    
    $scope.generateKeys = function(callback) {
      $scope.generating = true;
      var opts = {
        userid: $scope.user,
        primary: {
          nbits: Number($scope.keysize),
          flags: F.certify_keys | F.sign_data | F.auth | F.encrypt_comm | F.encrypt_storage,
          expire_in: Number($scope.expires)
        },
        subkeys: [
          {
            nbits: Number($scope.keysize)/2,
            flags: F.sign_data,
            expire_in: Number($scope.expires)
          },
          {
            nbits: Number($scope.keysize)/2,
            flags: F.encrypt_comm | F.encrypt_storage,
            expire_in: Number($scope.expires)
          }
        ]
      };
        
      kbpgp.KeyManager.generate(opts, function(err, person) {
        if (!err) {
          person.sign({}, function(err) {
            if (!err) {
              
              person.export_pgp_private_to_client({
                passphrase: $scope.passphrase
              }, function(err, pgp_private) {
                if (!err) {
                  person.export_pgp_public({}, function(err, pgp_public) {
                    if (!err) {
                      var data = {
                        priv: pgp_private,
                        pub: pgp_public
                      };
                      callback(null, data);
                      
                    }
                    else {
                      callback(err, null);
                    }
                  });  
                }
                else {
                  callback(err, null);
                }
              });
              
            }
            else {
              callback(err, null);
            }
          });
        }
        else {
          callback(err, null);
        }
      });
    };
    
    $scope.generate = function() {
      $scope.data = false;
      if ($scope.keysize < 2048) {
        if (!confirm('A keysize less than 2048 causes decryption errors if a message is signed using this key. Do you want to continue?')) {
          return false;
        }
      }
      $scope.generateKeys(function(err, data) {
        $scope.$apply(function() {
          $scope.data = data;
          $scope.generating =false;
        });
      });
      
    };
    
  }]);
  
  app.controller("encryptController", ["$scope", "$route", "$location", function($scope, $route, $location) {
    
    $scope.encryptMessage = function(callback) {
      kbpgp.KeyManager.import_from_armored_pgp({
        armored: $scope.recipient
      }, function(err, person) {
        if (!err) {
          
          if ($scope.sign) {
            kbpgp.KeyManager.import_from_armored_pgp({
              armored: $scope.privateKey
            }, function(err, signer) {
              if (!err) {
                if (signer.is_pgp_locked()) {
                  signer.unlock_pgp({
                    passphrase: $scope.passphrase
                  }, function(err) {
                    if (!err) {
                      var opts = {
                        msg: $scope.message,
                        encrypt_for: person,
                        sign_with: signer
                      };
                      
                      kbpgp.box(opts, function(err, result_string, result_buffer) {
                        if (!err) {
                          callback(null, result_string);
                        }
                        else {
                          callback(err, null);
                        }
                      })
                    }
                    else {
                      callback(err, null);
                    }
                  })
                }
                else {
                  // unlocked private key
                  var opts = {
                    msg: $scope.message,
                    encrypt_for: person
                  };
                  
                  kbpgp.box(opts, function(err, result_string, result_buffer) {
                    if (!err) {
                      callback(null, result_string);
                    }
                    else {
                      callback(err, null);
                    }
                  });
                }
              }
              else {
                callback(err, null);
              }
            })
          }
          else {
            // Not a signed message
            var opts = {
              msg: $scope.message,
              encrypt_for: person
            };
            
            kbpgp.box(opts, function(err, result_string, result_buffer) {
              if (!err) {
                callback(null, result_string);
              }
              else {
                callback(err, null);
              }
            });
          }
          
        }
        else {
          callback(err, null);
        }
      });
      
    };
    
    $scope.encrypt = function() {
      $scope.data = false;
      $scope.encryptMessage(function(err, result) {
        if (!err) {
          $scope.$apply(function() {
            $scope.data = {
              result: result
            };
          });
        }
        else {
          alert(err.message);
        }
      });
      
    };
  }]);
  
  app.controller("decryptController", ["$scope", "$route", "$location", function($scope, $route, $location) {
    
    $scope.decryptMessage = function(callback) {
      kbpgp.KeyManager.import_from_armored_pgp({
        armored: $scope.privateKey
      }, function(err, person) {
        if (!err) {
          
          if (person.is_pgp_locked()) {
            person.unlock_pgp({
              passphrase: $scope.passphrase
            }, function(err) {
              if (!err) {
                kbpgp.unbox({keyfetch: person, armored: $scope.message}, function(err, literals) {
                  if (err) {
                    console.log('broke here');
                    callback(err, null);
                  }
                  else {
                    var decrypted = literals[0].toString(),
                        ds = literals[0].get_data_signer(),
                        result = { message: decrypted };
                    
                    if (ds) {
                      var km = ds.get_key_manager();
                      result.fingerprint = km.get_pgp_fingerprint().toString('hex');
                    }
                    callback(null, result);
                  }
                });  
              }
              else {
                callback(err, null);
              }
            });
          }
          else {
            // No passphrase
            kbpgp.unbox({keyfetch: person, armored: $scope.message}, function(err, literals) {
              if (err) {
                callback(err, null);
              }
              else {
                var decrypted = literals[0].toString(),
                    ds = literals[0].get_data_signer(),
                    result = { message: decrypted };
                    
                if (ds) {
                  var km = ds.get_key_manager();
                  result.fingerprint = km.get_pgp_fingerprint().toString('hex');
                }
                callback(null, result);
              }
            });
          }
        }
        else {
          callback(err, null);
        }
      });
    };
    
    $scope.decrypt = function() {
      $scope.data = false;
      $scope.decryptMessage(function(err, result) {
        if (!err) {
          $scope.$apply(function() {
            $scope.data = {
              result: result
            };
          });
        }
        else {
          alert(err.message);
        }
      });
    };
  }]);
  
})(window);