$(function() {
  
  $("[data-add]").click(function() {
    $(this).toggleClass("disabled");
    $("form").toggleClass("hide");
    $(window).trigger('resize');
  });
  
})();