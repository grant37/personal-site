$(document).foundation();

/*
var stickyFooter = function() {
  var h = $(window).height(),
      ch = $(".content").height() + 50;
  if (h > ch) {
    $("footer").css({
      position: "fixed",
      width: "100%",
      left: "0",
      bottom: "0"
    });
  }
  else {
    $("footer").css({
      position: "static"
    });
  }
};

window.stickyFooter = stickyFooter;
*/
;(function(window) {

  Pizza.init(document.body,{
    donut: true,
    donut_inner_ratio: 0.1,
    percent_offset: 25,
  });
  
  
  //$(window).resize(stickyFooter);
  
  var editor = new MediumEditor("#postBody", {
    firstHeader: "h3",
    secondHeader: "h4",
    targetBlank: true,
    disableDoubleReturn: true,
    buttons: [
      'bold', 'italic', 'underline', 
      'anchor', 'header1', 'header2', 
      'quote', 'superscript', 'subscript',
      'unorderedlist', 'orderedlist', 'pre',
      'image', 'indent', 'outdent'
    ]
  });
  
  $("#postBody").on('input', function() {
    var content = editor.serialize();
    $("[name='postBody']").attr('value', content.postBody.value);
  });

  //stickyFooter();
  //$(window).trigger('resize');
})(window);