var User = require('../models/User'),
    Messages = require('../models/Messages');

/**
 * GET /me
 */
exports.getMe = function(req, res) {
  res.render('me', {
    title: "Control Panel"
  });
};

/**
 * GET /me/skills
 */
exports.getMySkills = function(req, res) {
  res.render('me/skills', {
    title: 'My Skills',
    linkDepth: 2,
    skills: req.user.me.skills
  });
};

/**
 * POST /me/skills
 * @param skill - String
 * @param percent - Integer
 */
exports.postMySkills = function(req, res, next) {
  req.assert('skill', "A skill title is required.").notEmpty();
  req.assert('percent', "A percent is required.").notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/me/skills');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var s = {
      skill: req.body.skill,
      percent: req.body.percent
    };
    user.me.skills.push(s);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That skill already exists'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/skills');
      }
      req.flash('success', { msg: 'Successfully added skill.'});
      res.redirect('/me/skills');
    });
    
  });
};

/**
 * GET /me/skills/:skillId/delete
 */
exports.getDeleteSkill = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.skills.id(req.params.skillId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delete skill.'});
        res.redirect('/me/skills');
      }
      req.flash('success', {msg: 'Successfully deleted skill.'});
      res.redirect('/me/skills');
    });
  });
};

/**
 * GET /me/experience
 */
exports.getMyExperience = function(req, res) {
  res.render('me/experience', {
    title: 'My Experience',
    linkDepth: 2,
    experiences: req.user.me.experiences
  });
};

/**
 * POST /me/experience
 * @param company - String
 * @param position - String
 * @param fromDate - String
 * @param toDate - String
 * @param description - String
 */
exports.postMyExperience = function(req, res, next) {
  req.assert('company', 'Company required.').notEmpty();
  req.assert('position', 'Position required.').notEmpty();
  req.assert('fromDate', 'From Date Required.').notEmpty();
  req.assert('des', 'Description Required.').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/me/experience');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var e = {
      company: req.body.company,
      position: req.body.position,
      fromDate: req.body.fromDate,
      toDate: req.body.toDate || 'present',
      description: req.body.des
    };
    user.me.experiences.push(e);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That already exists'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/experience');
      }
      req.flash('success', { msg: 'Successfully added skill.'});
      res.redirect('/me/experience');
    });
    
  });
};

/**
 * GET /me/experience/:expId/delete
 */
exports.getDeleteExperience = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.experiences.id(req.params.expId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delete experience.'});
        res.redirect('/me/experience');
      }
      req.flash('success', {msg: 'Successfully deleted experience.'});
      res.redirect('/me/experience');
    });
  });
};

/**
 * GET /me/featured
 */
exports.getMyFeatured = function(req, res) {
  res.render('me/featured', {
    title: 'My Featured Work',
    linkDepth: 2,
    features: req.user.me.featured
  });
};

/**
 * POST /me/featured
 * @param title - String
 * @param des - String
 * @param link - String
 */
exports.postMyFeatured = function(req, res, next) {
  req.assert('title', 'Title cannot be blank').notEmpty();
  req.assert('des', 'Description cannot be empty').notEmpty();
  req.assert('link', 'Link cannot be empty').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/me/featured');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var f = {
      title: req.body.title,
      description: req.body.des,
      link: req.body.link
    };
    user.me.featured.push(f);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That already exists'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/featured');
      }
      req.flash('success', { msg: 'Successfully added feature.'});
      res.redirect('/me/featured');
    });
    
  });
};

/**
 * GET /me/featured/:featuredId/delete
 */
exports.getDeleteFeatured = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.featured.id(req.params.featuredId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delete feature.'});
        res.redirect('/me/featured');
      }
      req.flash('success', {msg: 'Successfully deleted feature.'});
      res.redirect('/me/featured');
    });
  });
};

/**
 * GET /me/interests
 */
exports.getMyInterests = function(req, res) {
  res.render('me/interests', {
    title: 'My Interests',
    linkDepth: 2,
    interests: req.user.me.interests
  });
};

/**
 * POST /me/interests
 * @param title - String
 * @param amount - Integer
 */
exports.postMyInterests = function(req, res, next) {
  req.assert('title', 'The title cannot be blank').notEmpty();
  req.assert('amount', 'The amount cannot be blank').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    res.redirect('/me/interests');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var i = {
      title: req.body.title,
      amount: Number(req.body.amount)
    };
    user.me.interests.push(i);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That already exists'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/interests');
      }
      req.flash('success', { msg: 'Successfully added interest.'});
      res.redirect('/me/interests');
    });
    
  });
};

/**
 * GET /me/interests/:interestId/delete
 */
exports.getDeleteInterests = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.interests.id(req.params.interestId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delete interest.'});
        res.redirect('/me/interests');
      }
      req.flash('success', {msg: 'Successfully deleted interest.'});
      res.redirect('/me/interests');
    });
  });
};

/**
 * GET /me/education
 */
exports.getMyEducation = function(req, res) {
  res.render('me/education', {
    title: 'My Education',
    linkDepth: 2,
    education: req.user.me.education
  });
};

/**
 * POST /me/education
 * @param school - String
 * @param locale - String
 * @param fromDate - String
 * @param toDate - String
 * @param des - String
 */
exports.postMyEducation = function(req, res, next) {
  req.assert('school', 'School name cannot be blank').notEmpty();
  req.assert('locale', 'The location cannot be blank').notEmpty();
  req.assert('fromDate', 'The from date cannot be blank').notEmpty();
  req.assert('des', 'The description cannot be blank').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    res.redirect('/me/education');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var e = {
      school: req.body.school,
      location: req.body.locale,
      fromDate: req.body.fromDate,
      toDate: req.body.toDate || 'present',
      description: req.body.des
    };
    user.me.education.push(e);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That already exists'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/education');
      }
      req.flash('success', { msg: 'Successfully added.'});
      res.redirect('/me/education');
    });
    
  });
};

/**
 * GET /me/education/:educationId/delete
 */
exports.getDeleteEducation = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.education.id(req.params.educationId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delet.'});
        res.redirect('/me/education');
      }
      req.flash('success', {msg: 'Successfully deleted.'});
      res.redirect('/me/education');
    });
  });
};

/**
 * GET /me/accomplishments
 */
exports.getMyAccomplish = function(req, res) {
  res.render('me/accomplishments', {
    title: 'My Accomplishments',
    linkDepth: 2,
    accomplishments: req.user.me.accomplishments
  });
};

/**
 * POST /me/accomplishments
 * @param title - String
 * @param des - String (description)
 */
exports.postMyAccomplish = function(req, res, next) {
  req.assert('title', 'The title cannot be blank').notEmpty();
  req.assert('des', 'The description cannot be blank').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    res.redirect('/me/accomplishments');
  }
  
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    var a = {
      title: req.body.title,
      description: req.body.des
    };
    user.me.accomplishments.push(a);
    user.markModified('me');
    user.save(function(err) {
      if (err) {
        if (err.code === 11000) {
          req.flash('errors', { msg: 'That already exists.'});
        }
        console.error("Error: "+err);
        return res.redirect('/me/accomplishments');
      }
      req.flash('success', { msg: 'Successfully added accomplishment.'});
      res.redirect('/me/accomplishments');
    });
    
  });
};

/**
 * GET /me/accomplishments/:accomplishId/delete
 */
exports.getDeleteAccomplish = function(req, res) {
  User.findOne({ email: 't@grantcr.com'}, function(err, user) {
    user.me.accomplishments.id(req.params.accomplishId).remove();
    user.save(function(err) {
      if (err) {
        console.log(err);
        req.flash('errors', { msg: 'Failed to delete.'});
        res.redirect('/me/accomplishments');
      }
      req.flash('success', {msg: 'Successfully deleted.'});
      res.redirect('/me/accomplishments');
    });
  });
};

/**
 * GET /me/messages
 */
exports.getMessages = function(req, res) {
  Messages.find({}, function(err, message) {
    if (err) {
      console.error(err);
      req.flash('errors', { msg: 'Failed to get messages.'});
      res.redirect('/me');
    }
    res.render('me/messages', {
      title: 'My Messages',
      messages: message
    });
  });
};
/**
 * POST /me/messages/:messageId/delete
 * delete a message
 */
exports.getDeleteMessage = function(req, res) {
  Messages.findByIdAndRemove(req.params.messageId, function(err, data) {
    if (err) {
      req.flash('errors', { msg: 'Error deleting message'});
      return res.redirect('/me/messages');
    }
    req.flash('success', { msg: 'Successfully deleted message'});
    res.redirect('/me/messages');
  });
};