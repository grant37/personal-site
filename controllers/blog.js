var Blog = require('../models/Blog');

/**
 * GET /blog
 * Gets blog posts
 */
exports.getBlog = function(req, res, next) {
  Blog.find().limit(10).exec(function(err, blogPosts) {
    res.render("blog", {
      title: "Blog",
      posts: blogPosts
    });
  });
};

/**
 * GET /blog/:postSlug
 * Gets a post by the slug
 */
exports.getBlogPost = function(req, res) {
  Blog.findOne({slug: req.params.postSlug }, function(err, post) {
    if (err) {
      req.flash('errors', { msg: 'No such post '});
      res.redirect('/blog');
    }
    else if (!post) {
      res.redirect('/404');
    }
    else {
      res.render('blog/post', {
        title: 'Blog - '+post.title,
        activeBlogLink: true,
        post: post
      });
    }
  });
};

/**
 * GET /blog/:postSlug/delete
 * Deletes a blog post
 */
exports.getRemovePost = function(req, res) {
  Blog.findOneAndRemove({slug: req.params.postSlug }, function(err, post) {
    if (err) {
      console.log(err);
      res.redirect('/blog');
    }
    else if (!post) {
      req.flash('errors', { msg: 'No post matches that slug'});
      res.redirect('/blog');
    }
    else {
      req.flash('success', { msg: 'Successfully deleted post.'});
      res.redirect('/blog');
    }
  });
};

/**
 * GET /blog/add
 * Gets the add blog post page
 */
exports.getBlogAdd = function(req, res) {
  res.render("blog/add", {
    title: "Add Post"
  });
};

/**
 * POST /blog/add
 * Adds a new blog post
 * @param postTitle - String
 * @param postSlug - String
 * @param postBody - String
 */
exports.postBlogAdd = function(req, res, next) {
  req.assert('postTitle', 'You need to have a post title').notEmpty();
  req.assert('postSlug', 'There needs to be a post slug').notEmpty();
  req.assert('postBody', 'The post need content').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/blog/add');
  }
  
  var post = new Blog({
    title: req.body.postTitle,
    slug: req.body.postSlug,
    body: req.body.postBody
  });
  
  post.save(function(err) {
    if (err) {
      if (err.code == 11000) {
        req.flash('errors', { msg: 'Their is either already a post with that name or slug.'});
      }
      return res.redirect('/blog/add');
    }
    req.flash('success', { msg: 'Post successfully added'});
    res.redirect('/blog');
  });
};