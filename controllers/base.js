var passport = require('passport'),
    User = require('../models/User'),
    Message = require('../models/Messages'),
    fs = require('fs'),
    multiparty = require('multiparty'),
    pgp = require('../modules/pgp'),
    ExifImage = require('exif').ExifImage;

/**
 * GET /
 */
exports.getHome = function(req, res) {
  User.findOne({email: 't@grantcr.com'}, function(err, user) {
    if (err) {
      console.error(err);
      res.send(500);
    }
    if (!user) {
      res.send(500);
    }
    else {
      
      var skills = user.me.skills,
          interests = user.me.interests,
          experiences = user.me.experiences,
          education = user.me.education,
          accomplishments = user.me.accomplishments,
          features = user.me.featured;
          
      res.render("home", {
        title: "Home",
        skills: skills,
        experiences: experiences,
        features: features,
        interests: interests,
        education: education,
        accomplishments: accomplishments
      });
    }
  });  
};

/**
 * GET /404
 */
exports.get404 = function(req, res) {
  res.render('404', {
    title: 'Are you even trying?'
  });
};

/**
 * GET /contact
 */
exports.getContact = function(req, res) {
  res.render("contact", {
    title: "Contact"
  });
};

/**
 * POST /contact
 * @param email - String
 * @param myname - String
 * @param message - String
 */
exports.postContact = function(req, res) {
  req.assert('email', 'Your email cannot be blank!').notEmpty();
  req.assert('myname', 'Your name cannot be blank!').notEmpty();
  req.assert('message', 'You at least need to have something to contact me about.').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/contact');
  }
  
  var message = new Message({
    from_email: req.body.email,
    from_name: req.body.myname,
    message: req.body.message
  });
  
  message.save(function(err) {
    if (err) {
      req.flash('errors', { msg: 'There was an error sending the message.'});
      console.error(err);
      return res.redirect('/contact');
    }
    req.flash('success', { msg: 'Your message has been successfully sent.'});
    res.redirect('/contact');
  });
};

/**
 * GET /login
 */
exports.getLogin = function(req, res) {
  res.render("login", {
    title: "Login"
  });
};

/**
 * GET /logout
 */
exports.getLogout = function(req, res) {
  req.logout();
  res.redirect('/');
};

/**
 * POST /login
 * @param email
 * @param password
 */
exports.postLogin = function(req, res, next) {
  req.assert('email', 'A valid email address was not entered').isEmail();
  req.assert('password', 'The password cannot be empty').notEmpty();
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    res.redirect('/login');
  }
  
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err);
    if (!user) {
      //req.flash('errors', { msg: 'Invalid email or password.'});
      return res.redirect('/login');
    }
    req.login(user, function(err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
  
};

/**
 * GET /register
 */
exports.getRegister = function(req, res) {
  if (req.user) return res.redirect('/');
  res.render("reg", {
    title: "Register"
  });
};

/**
 * POST /register
 * @param email
 * @param password
 */
exports.postRegister = function(req, res, next) {
  req.assert('email', 'A valid email address is required').isEmail();
  req.assert('password', 'Password cannot be blank.').notEmpty();
  req.assert('password', 'Password needs to be at least 6 characters').len(6);
  
  var errors = req.validationErrors();
  
  if (errors) {
    req.flash('errors', errors);
    res.redirect('/register');
  }
  
  var user = new User({
    email: req.body.email,
    password: req.body.password
  });
  
  user.save(function(err) {
    if (err) {
      console.error(err);
    }
    req.logIn(user, function(err) {
      if (err) { 
        req.flash('error', err);
      }
      req.flash('success', 'You have successfully registered');
      return res.redirect('/');
    });
  });
};

/**
 * GET /exif
 */
exports.getExif = function(req, res) {
  res.render("exif", {
    title: "Exif Viewer"
  });
};

/**
 * POST /exif
 */
exports.postExif = function(req, res) {
  
  var form = new multiparty.Form({
    uploadDir: "uploads/"
  });
      
  
  form.parse(req, function(err, fields, files) {
    
    for (var k in files) {
      var uploadedFile = files[k][0];
      
      try {
        new ExifImage({ image: uploadedFile.path }, function(err, exifData) {
          if (err) {
            console.error(err);
            req.flash('errors', { msg: 'Failed to get the exif data' });
            res.redirect("/exif");
          }
          else {
            
            fs.unlinkSync(uploadedFile.path);
            
            res.render("exif", {
              title: "Exif Viewer",
              exifData: JSON.stringify(exifData, null, 2)
            });
          }
        });
      }
      catch (err) {
        console.error(err);
        req.flash('errors', { msg: 'Failed to get the exif data' });
        res.redirect("/exif");
      }
      
      
    }
    
  });
};

/**
 * GET /pgp
 */
exports.getPgp = function(req, res) {
  res.render("pgp", {
    title: "PGP Playground"
  });
};

/**
 * POST /pgp
 * 
 * NOT USED
 */
exports.postPgp = function(req, res) {
  var action = req.body.action;
  switch (action) {
    
    case 'gen':
      pgp.generateKeys(req.body.user, req.body.passphrase, req.body.keysize, req.body.expires,
        function(err, privateKey, publicKey) {
          if (!err) {
            res.send({
              priv: privateKey,
              pub: publicKey
            });
          }
          else {
            res.send(500);
          }
      });
      break;
    case 'enc':
      var signData = null;
      if (req.body.passphrase && req.body.privateKey) {
        signData = { passphrase: req.body.passphrase, privateKey: req.body.privateKey };
      }
      pgp.encryptMessage(req.body.recipient, req.body.message, signData, function(err, result) {
        if (!err) {
          res.send({result: result});
        }
        else {
          res.send(500);
        }
      });
      break;
    case 'dec':
      pgp.decryptMessage(req.body.privateKey, req.body.passphrase, req.body.message, function(err, result) {
        if (!err) {
          res.send({result: result});
        }
        else {
          res.send(500);
        }
      });
      break;
    default:
      break;
  }
};