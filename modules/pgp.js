var kbpgp = require('kbpgp'),
    F = kbpgp["const"].openpgp;

module.exports = {
  /**
   * generateKeys
   * Generates private/public pgp keys
   * 
   * @param user - String
   * @param passphrase - String
   * @param keysize - Integer
   * @param expires - Integer
   * @param callback - Function(err, privateKey, publicKey)
   */
  generateKeys: function(user, passphrase, keysize, expires, callback) {
    var opts = {
      userid: user,
      primary: {
        nbits: Number(keysize),
        flags: F.certify_keys | F.sign_data | F.auth | F.encrypt_comm | F.encrypt_storage,
        expire_in: Number(expires)
      },
      subkeys: [
        {
          nbits: Number(keysize)/2,
          flags: F.sign_data,
          expire_in: Number(expires)
        },
        {
          nbits: Number(keysize)/2,
          flags: F.encrypt_comm | F.encrypt_storage,
          expire_in: Number(expires)
        }
      ]
    };
    
    kbpgp.KeyManager.generate(opts, function(err, person) {
      if (!err) {
        person.sign({}, function(err) {
          if (!err) {
            
            person.export_pgp_private_to_client({
              passphrase: passphrase
            }, function(err, pgp_private) {
              if (!err) {
                person.export_pgp_public({}, function(err, pgp_public) {
                  if (!err) {
                    callback(null, pgp_private, pgp_public);
                  }
                  else {
                    callback(err, null, null);
                  }
                });  
              }
              else {
                callback(err, null, null);
              }
            });
            
          }
          else {
            callback(err, null, null);
          }
        });
      }
      else {
        callback(err, null, null);
      }
    });
    
  },
  /**
   * encryptMessage
   * Encrypts a message using PGP
   * 
   * @param publicKey - String
   * @param message - String
   * @param signData - Object (privateKey, passphrase)
   * @param callback - Function(err, result)
   */
  encryptMessage: function(publicKey, message, signData, callback) {
    kbpgp.KeyManager.import_from_armored_pgp({
      armored: publicKey
    }, function(err, person) {
      if (!err) {
        
        if (signData) {
          kbpgp.KeyManager.import_from_armored_pgp({
            armored: signData.privateKey
          }, function(err, signer) {
            if (!err) {
              if (signer.is_pgp_locked()) {
                signer.unlock_pgp({
                  passphrase: signData.passphrase
                }, function(err) {
                  if (!err) {
                    var opts = {
                      msg: message,
                      encrypt_for: person,
                      sign_with: signer
                    };
                    
                    kbpgp.box(opts, function(err, result_string, result_buffer)  {
                      if (!err) {
                        callback(null, result_string);
                      }
                      else {
                        callback(err, null);
                      }
                    });
                  }
                  else {
                    callback(err, null);
                  }
                });
              }
              else {
                var opts = {
                  msg: message,
                  encrypt_for: person,
                  sign_with: signer
                }
                
                kbpgp.box(opts, function(err, result_string, result_buffer)  {
                  if (!err) {
                    callback(null, result_string);
                  }
                  else {
                    callback(err, null);
                  }
                });
              }
            }
            else {
              callback(err, null);
            }
          });
        }
        else {
        
          var opts = {
            msg: message,
            encrypt_for: person
          };
          
          kbpgp.box(opts, function(err, result_string, result_buffer)  {
            if (!err) {
              callback(null, result_string);
            }
            else {
              callback(err, null);
            }
          });
        }
      }
      else {
        callback(err, null);
      }
    });
  },
  /**
   * decryptMessage
   * Decrypts a message using PGP
   * 
   * @param privateKey - String
   * @param passphrase - String
   * @param message - String
   * @param callback - Function(err, result)
   */
  decryptMessage: function(privateKey, passphrase, message, callback) {
    kbpgp.KeyManager.import_from_armored_pgp({
      armored: privateKey
    }, function(err, person) {
      if (!err) {
        
        if (person.is_pgp_locked()) {
          person.unlock_pgp({
            passphrase: passphrase
          }, function(err) {
            if (!err) {
              kbpgp.unbox({keyfetch: person, armored: message}, function(err, literals) {
                if (err) {
                  callback(err, null);
                }
                else {
                  var decrypted = literals[0].toString(),
                      ds = literals[0].get_data_signer(),
                      result = { message: decrypted };
                  
                  if (ds) {
                    var km = ds.get_key_manager();
                    result.fingerprint = km.get_pgp_fingerprint().toString('hex');
                  }
                  
                  callback(null, result);
                }
              });
            }
            else {
              callback(err, null);
            }
          });
        }
        else {
          kbpgp.unbox({keyfetch: person, armored: message}, function(err, literals) {
            if (err) {
              callback(err, null);
            }
            else {
              var decrypted = literals[0].toString(),
                  ds = literals[0].get_data_signer(),
                  result = { message: decrypted };
              
              if (ds) {
                var km = ds.get_key_manager();
                result.fingerprint = km.get_pgp_fingerprint().toString('hex');
              }
              
              callback(null, result);
            }
          });
        }
        
      }
      else {
        callback(err, null);
      }
    });
  },
};