/**
 * Module dependencies.
 */
var express = require('express'),
    flash = require('express-flash'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    expressValidator = require('express-validator'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    favicon = require('serve-favicon'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session),
    csrf = require('./config/csrf');

/**
 * Controllers
 */
var baseController = require('./controllers/base'),
    blogController = require('./controllers/blog'),
    myController = require('./controllers/me');


/**
 * API / secrets
 */
var secrets = require('./config/secrets'),
    passportConf = require('./config/passport');
    
/**
 * Express
 */
var app = express();

/**
 * Mongo / Mongoose
 */
mongoose.connect(secrets.db, {w: -1});
mongoose.connection.on('error', function(err) {
  console.error(err);
  console.error("MongoDB Connection Error: Make sure that MongoDB is running\n");  
});
 
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(require('connect-assets')({
  src: 'public',
  helperContext: app.locals
}));
//app.use(favicon());
app.use(morgan('dev'));
app.use(cookieParser('YouShallNotPassWord'));
app.use(bodyParser());
app.use(expressValidator());
app.use(methodOverride());
app.use(session({
  secret: secrets.sessionSecret,
  store: new MongoStore({
    db: mongoose.connection.db,
    auto_reconnect: true
  })
}));
app.use(csrf);
app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
  res.locals.user = req.user;
  if (req.csrfToken !== undefined) {
    res.locals.token = req.csrfToken();
  }
  res.locals.secrets = secrets;
  res.set('X-Powered-By', 'Electricity');
  next();
});
app.use(flash());
app.use(express.static(__dirname + '/public'));
 
/**
 * Routers
 */
var mainRouter = express.Router(),
    blogRouter = express.Router(),
    meRouter = express.Router();

/**
 * Main Router
 */
mainRouter.get("/", baseController.getHome);
mainRouter.get("/pgp", baseController.getPgp);
mainRouter.get("/pgp/:subPage", baseController.getPgp);
mainRouter.post("/pgp", baseController.postPgp); // Dont use for security
mainRouter.get("/exif", baseController.getExif);
mainRouter.post("/exif", baseController.postExif);
mainRouter.get("/404", baseController.get404);
mainRouter.get("/login", baseController.getLogin);
mainRouter.post("/login", baseController.postLogin);
mainRouter.get("/logout", baseController.getLogout);
mainRouter.get("/contact", baseController.getContact);
mainRouter.post("/contact", baseController.postContact);

/**
 * Blog Router
 */
blogRouter.get("/", blogController.getBlog);
blogRouter.get("/add", passportConf.isAuthenticated, blogController.getBlogAdd);
blogRouter.post("/add", passportConf.isAuthenticated, blogController.postBlogAdd);
blogRouter.get("/:postSlug", blogController.getBlogPost);
blogRouter.get("/:postSlug/delete", passportConf.isAuthenticated, blogController.getRemovePost);

/**
 * Me Router
 */
meRouter.use(passportConf.isAuthenticated);
meRouter.get("/", myController.getMe);
meRouter.get("/skills", myController.getMySkills);
meRouter.post("/skills", myController.postMySkills);
meRouter.get("/skills/:skillId/delete", myController.getDeleteSkill);
meRouter.get("/experience", myController.getMyExperience);
meRouter.post("/experience", myController.postMyExperience);
meRouter.get("/experience/:expId/delete", myController.getDeleteExperience);
meRouter.get("/featured", myController.getMyFeatured);
meRouter.post("/featured", myController.postMyFeatured);
meRouter.get("/featured/:featuredId/delete", myController.getDeleteFeatured);
meRouter.get("/interests", myController.getMyInterests);
meRouter.post("/interests", myController.postMyInterests);
meRouter.get("/interests/:interestId/delete", myController.getDeleteInterests);
meRouter.get("/education", myController.getMyEducation);
meRouter.post("/education", myController.postMyEducation);
meRouter.get("/education/:educationId/delete", myController.getDeleteEducation);
meRouter.get("/accomplishments", myController.getMyAccomplish);
meRouter.post("/accomplishments", myController.postMyAccomplish);
meRouter.get("/accomplishments/:accomplishId/delete", myController.getDeleteAccomplish);
meRouter.get("/messages", myController.getMessages);
meRouter.get("/messages/:messageId/delete", myController.getDeleteMessage);

app.use("/", mainRouter);
app.use("/blog", blogRouter);
app.use("/me", meRouter);

app.use(function(req, res) {
  res.status(404).redirect('/404');
});


/**
 * Start Server
 */
app.listen(app.get('port'), function() {
  console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});