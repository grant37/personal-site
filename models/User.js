var mongoose = require('mongoose'),
    bcrypt = require('bcrypt-nodejs'),
    amounts = [1,2,3,4,5,6,7,8,9,10];
    
/**
 * Accomplish SubDoc
 */
var accomplish = new mongoose.Schema({
  title: String,
  description: String
});

/**
 * Education SubDoc
 */
var education = new mongoose.Schema({
  school: String,
  location: String,
  fromDate: String,
  toDate: { type: String, default: 'present' },
  description: String
});

/**
 * Experience SubDoc
 */
var experience = new mongoose.Schema({
  company: String,
  position: String,
  fromDate: String,
  toDate: { type: String, default: "present" },
  description: String
});

/**
 * Featured SubDoc
 */
var featured = new mongoose.Schema({
  title: String,
  description: String,
  link: String
});

/**
 * Interest SubDoc
 */
var interest = new mongoose.Schema({
  title: String,
  amount: { type: Number, enum: amounts }
});

/**
 * Skills SubDoc
 */
var skill = new mongoose.Schema({
  skill: { type: String, unique: true },
  percent: String
});


/**
 * userSchema
 * Our user schematic
 */
var userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
  
  me: {
    skills: [skill],
    interests: [interest],
    featured: [featured],
    experiences: [experience],
    education: [education],
    accomplishments: [accomplish]
  }
});

/**
 * Hash the password for security.
 */

userSchema.pre('save', function(next) {
  var user = this;
  var SALT_FACTOR = 5;

  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_FACTOR, function(err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('User', userSchema);
