var mongoose = require("mongoose"),
    amounts = [1,2,3,4,5,6,7,8,9,10],
    ObjectId = mongoose.Schema.Types.ObjectId;

/**
 * Accomplish SubDoc
 */
var accomplish = new mongoose.Schema({
  title: String,
  description: String
});

/**
 * Education SubDoc
 */
var education = new mongoose.Schema({
  school: String,
  location: String,
  fromDate: String,
  toDate: { type: String, default: 'present' },
  description: String
});

/**
 * Experience SubDoc
 */
var experience = new mongoose.Schema({
  company: String,
  position: String,
  fromDate: String,
  toDate: { type: String, default: "present" },
  description: String
});

/**
 * Featured SubDoc
 */
var featured = new mongoose.Schema({
  title: String,
  description: String,
  link: String
});

/**
 * Interest SubDoc
 */
var interest = new mongoose.Schema({
  title: String,
  amount: { type: Number, enum: amounts }
});

/**
 * Skills SubDoc
 */
var skill = new mongoose.Schema({
  skill: { type: String, unique: true },
  percent: String
});

var meSchema = new mongoose.Schema({
  user_id: ObjectId,
  skills: [skill],
  experience: [experience],
  featured: [featured],
  interests: [interest],
  education: [education],
  accomplishments: [accomplish]
});

module.exports = mongoose.model("Me", meSchema);