var mongoose = require("mongoose");

var blogSchema = new mongoose.Schema({
  title: { type: String, unique: true },
  author: { type: String, default: 'Tyler' }, 
  body: String,
  slug: { type: String, unique: true },
  dateCreated: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Blog', blogSchema);