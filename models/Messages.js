var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
  from_email: String,
  from_name: String,
  message: String
});

module.exports = mongoose.model('Message', messageSchema);